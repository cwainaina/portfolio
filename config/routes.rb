Rails.application.routes.draw do
  root 'welcome#index'
  resources :posts
  resources :pages
end
